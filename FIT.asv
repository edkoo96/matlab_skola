function varargout = FIT(varargin)
% FIT M-file for FIT.fig
%      FIT, by itself, creates a new FIT or raises the existing
%      singleton*.
%
%      H = FIT returns the handle to a new FIT or the handle to
%      the existing singleton*.
%
%      FIT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FIT.M with the given input arguments.
%
%      FIT('Property','Value',...) creates a new FIT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before FIT_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to FIT_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help FIT

% Last Modified by GUIDE v2.5 20-Feb-2017 11:20:11

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @FIT_OpeningFcn, ...
                   'gui_OutputFcn',  @FIT_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before FIT is made visible.
function FIT_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to FIT (see VARARGIN)

% Choose default command line output for FIT
handles.output = hObject;
handles.fitfun = 1;
handles.StupPol = 5;
global data
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes FIT wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = FIT_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in openbutton.
function openbutton_Callback(hObject, eventdata, handles)
% hObject    handle to openbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global data
[filename,pathname] = uigetfile({'*.mat';'*.txt';'*.*'},'File Selector');
data = importdata(strcat(pathname,filename));
if isempty(data)
    set(handles.textdata,'String','Data is not imported!');
else
    set(handles.textdata,'String',filename);
end


% --- Executes on selection change in popupmenu.
function popupmenu_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu

val=get(hObject,'Value');
switch val
    case 1
        handles.fitfun = 1;
    case 2
        handles.fitfun = 2;
end

guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function popupmenu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in fitbutton.
function fitbutton_Callback(hObject, eventdata, handles)
% hObject    handle to fitbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global data
[k,l]=size(data);

if k==2
    data=data'
end
[k,l]=size(data);
xdata=data(:,1);
ydata=data(:,2);
plot(xdata,ydata,'o');


fitfun = handles.fitfun;
StupPol = handles.StupPol;
xx=linspace(xdata(1,1),xdata(k,1),10*k);
if fitfun==1
    p=polyfit(xdata,ydata,StupPol);
    yy1=polyval(p,xx);
    plot(xdata,ydata,'o',xx,yy1,'r');
else
    yy2=spline(xdata,ydata,'o',xdata,ydata,xx);
    plot(xx,yy2,'g');
end


function stuppoltext_Callback(hObject, eventdata, handles)
% hObject    handle to stuppoltext (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of stuppoltext as text
%        str2double(get(hObject,'String')) returns contents of stuppoltext as a double

StupPol = str2double(get(hObject,'String'));
if isnan(StupPol)
    errordlg('Enter valid numeric value','BAD INPUT','modal');
    uicontrol(hObject);
    set(hObject,'String','5');
    return
end
handles.StupPol=StupPol;
guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function stuppoltext_CreateFcn(hObject, eventdata, handles)
% hObject    handle to stuppoltext (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
check = get(hObject,'Value');
if check == 0
    hold off
else 
    hold on
end
guidata(hObject);

% Hint: get(hObject,'Value') returns toggle state of checkbox1
